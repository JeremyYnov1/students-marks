package com.bourgne.main;

public class Main {

	public static void main(String[] args) {
		StudentsMarks studentsMarks = new StudentsMarks();
		
		// Populate the students marks with the user input
		studentsMarks.PopulateStudentsMarks();
		
		// Display the options to the user for the students marks
		studentsMarks.DisplayOptions();
	}
}
