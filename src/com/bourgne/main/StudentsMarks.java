package com.bourgne.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

import com.bourgne.models.Student;

public class StudentsMarks {
	private HashMap<Student, List<Double>> studentsMarks;
	
	public StudentsMarks() {
		setStudentsMarks(new HashMap<Student, List<Double>>());
	}

	public HashMap<Student, List<Double>> getStudentsMarks() {
		return studentsMarks;
	}

	public void setStudentsMarks(HashMap<Student, List<Double>> studentsMarks) {
		this.studentsMarks = studentsMarks;
	}
	
	// Ask the user to enter a student name and mark until he press "q"
	public void PopulateStudentsMarks() {
		System.out.println("Enter the students marks.");
		System.out.println("-------------------------");
		
		Scanner sc = new Scanner(System.in);
		String userInput = "";
		
		while (!userInput.equals("q") && !userInput.equals("Q")) {
			String studentFirstname, studentLastname;
			double studentMark = 0;
			
			System.out.println("Student first name:");
			studentFirstname = sc.next();
			
			System.out.println("Student last name:");
			studentLastname = sc.next();
			
			// We reset the buffer
			sc.nextLine();
			
			System.out.println("Student mark:");
			studentMark = sc.nextDouble();
			
			// 1 - If the student exist, we add the mark to him
			boolean isExistingStudent = IsStudentInList(studentFirstname, studentLastname);
			
			// 1 - If the student does not exist, we create one
			if (!isExistingStudent) {
				// Create the student
				System.out.println("We create the student.");
				AddStudentWithMark(studentFirstname, studentLastname, studentMark);
			} else {
				// We add the mark to the existing one
				System.out.println("We add the mark to the existing student.");
				AddMarkToExistingStudent(studentFirstname, studentLastname, studentMark);
			}
			
			System.out.println("Do you want to quit entering student grades? (if yes press 'q' or 'Q', if no press another key to continue):");
			userInput = sc.next();
		}
		System.out.println("Program ended.");
		System.out.println("-------------------------");
	}
	
	public boolean IsStudentInList(String firstname, String lastname) {
		// Check if the hashmap is empty
		if (!studentsMarks.isEmpty()) {
			// We loop through and we check if it is the same or not
			for (Entry<Student, List<Double>> studentMark : studentsMarks.entrySet()) {
				Student currentStudent = studentMark.getKey();
				
				if (currentStudent.getFirstname().equals(firstname) 
						&& currentStudent.getLastname().equals(lastname)) {
					return true;
				}
			}	
		}
		return false;
	}
	
	public void AddStudentWithMark(String firstname, String lastname, double mark) {
		Student student = new Student(firstname, lastname);
		
		// Add the values to the hashmap
		List<Double> marks = new ArrayList<Double>();
		marks.add(mark);
		
		studentsMarks.put(student, marks);
	}
	
	public void AddMarkToExistingStudent(String firstname, String lastname, double mark) {
		for (Entry<Student, List<Double>> studentMarks : studentsMarks.entrySet()) {
			Student currentStudent = studentMarks.getKey();
			
			if (currentStudent.getFirstname().equals(firstname) 
					&& currentStudent.getLastname().equals(lastname)) {
				
				List<Double> marks = studentMarks.getValue();
				marks.add(mark);
				
				studentMarks.setValue(marks);
			}
		}	
	}
	
	// Loop 2:
	//  * After quiting the first loop
	//	* Ask him to enter one of these keys:
	public void DisplayOptions() {
		
		Scanner loopSc = new Scanner(System.in);
		String loopInput = "";
		
		while (!loopInput.equals("q") && !loopInput.equals("Q")) {
			
			System.out.println("Display options");
			System.out.println("-------------------------");
			System.out.println("    --> Press 'a' to display all the marks.");
			System.out.println("    --> Press 'b' to display one mark for a specific student.");
			System.out.println("    --> Press 'c' to display the total number of marks.");
			System.out.println("    --> Press 'd' to display the average of the marks.");
			System.out.println("    --> Press 'e' to quit the program.");

			String userInput = "";
			Scanner sc = new Scanner(System.in);
			userInput = sc.next();
			
			switch (userInput) {
				case "a":
					DisplayAllMarks();
					break;
				case "b":
					// Reset the buffer
					sc.nextLine();
					
					System.out.println("Firstname of the student:");
					String firstname = sc.next();
					System.out.println("Lastname of the student:");
					String lastname = sc.next();
					
					sc.nextLine();
					System.out.println("Mark of the student:");
					double mark = sc.nextDouble();
					
					DisplayOneMark(firstname, lastname, mark);
					break;
				case "c":
					DisplayNumberOfMarks();
					break;
				case "d":
					DisplayAverageOfMarks();
					break;
				case "e":
					loopInput = "q";
					break;
				default:
					System.out.println("I do not understand your input.");
					break;
			}
			
			if (loopInput != "q") {
				System.out.println("Do you want to leave the options? (if yes press 'q' or 'Q', if no press another key to continue):");
				loopInput = loopSc.next();
			}
		}
		
		System.out.println("Program ended.");
		System.out.println("-------------------------");
	}
	
	// Display all the marks
	private void DisplayAllMarks() {
		for (Entry<Student, List<Double>> student : studentsMarks.entrySet()) {
			Student currentStudent = student.getKey();
			List<Double> studentMark = student.getValue();
			
			System.out.println("Student: " + currentStudent.getFullname() + "; Marks: " + studentMark.toString());
		}
	}
	
	// Display one mark for a specific student
	private void DisplayOneMark(String firstname, String lastname, double mark) {
		if (!studentsMarks.isEmpty()) {
			for (Entry<Student, List<Double>> studentMarks : studentsMarks.entrySet()) {
				Student currentStudent = studentMarks.getKey();
				
				if (currentStudent.getFirstname().equals(firstname) 
						&& currentStudent.getLastname().equals(lastname)) {
					
					List<Double> marks = studentMarks.getValue();

					if (marks.contains(mark)) {
						System.out.println("The mark " + mark + " is in the list of student marks.");
					} else {
						System.out.println("The mark " + mark + " isn't in the list of student marks.");
					}
				}
			}
		} else {
			System.out.println("The list is empty.");
		}
			
	}
	
	// Display the total number of marks
	private void DisplayNumberOfMarks() {
		int totalOfMarks = 0;
		for (Entry<Student, List<Double>> studentMarks : studentsMarks.entrySet()) {
			List<Double> marks = studentMarks.getValue();
			totalOfMarks += marks.size();
		}
		System.out.println("There is a total of " + totalOfMarks + " marks in the students marks set");
	}
	
	// Display the average of the marks
	private void DisplayAverageOfMarks() {
		int totalOfMarks = 0;
		double totalMarksNumber = 0;
		for (Entry<Student, List<Double>> studentMarks : studentsMarks.entrySet()) {
			List<Double> marksList = studentMarks.getValue();
			
			for (int i = 0; i < marksList.size(); i++) {
				totalMarksNumber += marksList.get(i);
			}
			totalOfMarks += marksList.size();
		}
		
		double average = totalMarksNumber / totalOfMarks;
		System.out.println("The average of all the marks is: " + average + ".");
	}
}
